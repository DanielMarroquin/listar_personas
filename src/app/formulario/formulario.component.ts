import { Component, ElementRef, ViewChild } from '@angular/core';
import { LoginService } from '../LoginService.service';
import { Persona } from '../persona.model';
import { PersonasServices } from '../personas.service';


@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css'],
})
export class FormularioComponent {
  // @Output() personaCreada = new EventEmitter<Persona>();
  // nombreInput:string = '';
  // apellidoInput:string = '';
  @ViewChild('nombreInput') nombreInput: ElementRef;
  @ViewChild('apellidoInput') apellidoInput: ElementRef;

  constructor(private loginService:LoginService,
              private personasService:PersonasServices){}

  ngOnInit(){

  }

  agregarPersona(){
    let persona1 = new Persona(
      this.nombreInput.nativeElement.value,
      this.apellidoInput.nativeElement.value);
    //   this.loginService.enviarMensajeConsola(`Enviamos persona: ${persona1.nombre} Apellido ${persona1.apellido}`);
    // this.personaCreada.emit(persona1);
    this.personasService.agregarPersona(persona1);
  }

}
