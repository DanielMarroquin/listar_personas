import { Component, OnInit } from '@angular/core';
import { LoginService } from './LoginService.service';
import { Persona } from './persona.model';
import { PersonasServices } from './personas.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  titulo = 'Listado de Personas'; 
  personas: Persona[] = [];

  constructor(private loginService: LoginService,
              private personasService: PersonasServices) {
    
  }
  ngOnInit(): void {
    this.personas = this.personasService.personas; 
  }

}
