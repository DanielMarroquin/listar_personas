import { Injectable } from "@angular/core";
import { LoginService } from "./LoginService.service";
import { Persona } from "./persona.model";

@Injectable()
export class PersonasServices {
    personas: Persona[] = [
        new Persona('Daniel', 'Marroquin'), 
        new Persona('Eduardo','Caicedo'),
        new Persona('Peter', 'Parker')
      ];

    constructor(private loginService: LoginService){}
    
    agregarPersona(persona: Persona){
        this.loginService.enviarMensajeConsola(`Agregamos persona: ${persona.nombre}`);    
        this.personas.push ( persona);
      }

}